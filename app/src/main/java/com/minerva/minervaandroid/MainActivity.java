package com.minerva.minervaandroid;

import android.content.ActivityNotFoundException;
import android.content.ComponentName;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.app.Activity;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.webkit.PermissionRequest;
import android.webkit.ValueCallback;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.webkit.WebView;
import android.app.ActionBar;
import android.widget.Toast;

import com.microblink.activity.ScanActivity;
import com.microblink.recognizers.BaseRecognitionResult;
import com.microblink.recognizers.RecognitionResults;
import com.minerva.minervaandroid.scan_id.ocr.MRPScanActivity;
import com.minerva.minervaandroid.scan_id.ocr.ResultActivity;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    private static final int MY_BLINK_ID_REQUEST_CODE = 0x101;
    private static final String TAG = MainActivity.class.getSimpleName();
    private WebView mWebView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupToolbar();

        loadHtml();
    }

    /**
     * setup toolbar
     */
    private void setupToolbar() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //change title color
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white));
        //set title
        getSupportActionBar().setTitle(getString(R.string.app_name));

    }
    public static int REQUEST_SELECT_FILE;

    private void loadHtml() {
        mWebView = (WebView) findViewById(R.id.wv);
        mWebView.setWebViewClient(new WebViewClient());

        WebSettings settings = mWebView.getSettings();
        settings.setJavaScriptEnabled(true);
        settings.setLoadWithOverviewMode(true);
        settings.setUseWideViewPort(true);

//        private ValueCallback<Uri[]> mUploadMessage;
//        private FilePickerDialog dialog;

        final int TAKE_PICTURE = 3;

        // Render the HTML file on WebView
        mWebView.loadUrl("http://new.techyardnepal.com/new/");
        mWebView.setWebChromeClient( new WebChromeClient() {
            @Override
            public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
                // Double check that we don't have any existing callbacks
               //Toast.makeText( getApplicationContext(), "Dialog", Toast.LENGTH_LONG );

                boolean isMultipleTypes = false;
                Set<String> mimeTypes = new HashSet<String>();
                String[] mimetypespec = new String[]{"image/*", "imgage/jpeg", "image/jpg"};
                for (String spec : mimetypespec) {
                    String[] splitSpec = spec.split("[,;\\s]");
                    for (String s : splitSpec) {
                        if (s.startsWith(".")) {
                            String t = MimeTypeMap.getSingleton().getMimeTypeFromExtension(s.substring(1));
                            if (t != null) mimeTypes.add(t);
                        } else if (s.contains("/")) {
                            mimeTypes.add(s);
                        }
                    }
                }

                if (mimeTypes.isEmpty()) mimeTypes.add("*/*");

                boolean useCamera = false;
                boolean useVideo = false;

                if (true) {
                    for (String type : mimeTypes) {
                        if (type.equals("*/*")) {
                            useCamera = true;
                            useVideo = true;
                        } else if (type.equals("image/*") || type.equals("image/jpeg") || type.equals("image/jpg")) {
                            useCamera = true;
                        } else if (type.startsWith("video/")) {
                            useVideo = true;
                        }
                    }
                }

                List<Intent> directCaptureIntents = new ArrayList<>();

                PackageManager packageManger = MainActivity.this.getPackageManager();
                if (useCamera) {
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                    String imageFileName = "IMG_" + timeStamp + ".jpg";
                    File storageDir = Environment.getExternalStoragePublicDirectory(
                            Environment.DIRECTORY_PICTURES);
                    File captureFile = new File(storageDir, imageFileName);

                    Uri captureUrl = Uri.fromFile(captureFile);

                    if (captureUrl != null) {
                        Intent captureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        List<ResolveInfo> resolveList = packageManger.queryIntentActivities(captureIntent, 0);
                        for (ResolveInfo resolve : resolveList) {
                            String packageName = resolve.activityInfo.packageName;
                            Intent intent = new Intent(captureIntent);
                            intent.setComponent(new ComponentName(resolve.activityInfo.packageName, resolve.activityInfo.name));
                            intent.setPackage(packageName);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(captureFile));
                            MainActivity.this.setDirectUploadImageUri(captureUrl);
                            directCaptureIntents.add(intent);
                        }
                    }
                }

                if (useVideo) {
                    Intent captureIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                    List<ResolveInfo> resolveList = packageManger.queryIntentActivities(captureIntent, 0);
                    for (ResolveInfo resolve : resolveList) {
                        String packageName = resolve.activityInfo.packageName;
                        Intent intent = new Intent(captureIntent);
                        intent.setComponent(new ComponentName(resolve.activityInfo.packageName, resolve.activityInfo.name));
                        intent.setPackage(packageName);
                        directCaptureIntents.add(intent);
                    }
                }

                Intent documentIntent = new Intent();
                documentIntent.setAction(Intent.ACTION_GET_CONTENT);
                documentIntent.addCategory(Intent.CATEGORY_OPENABLE);

                if (mimeTypes.size() == 1) {
                    documentIntent.setType(mimeTypes.iterator().next());
                } else {
                    documentIntent.setType("*/*");

                    // If running kitkat or later, then we can specify multiple mime types
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                        documentIntent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes.toArray(new String[mimeTypes.size()]));
                    }
                }

                // INTENT_ALLOW_MULTIPLE can be used starting API 18. But we should only get multiple=true
                // starting in Lollipop anyway.
                if (/*multiple*/ true && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
                    documentIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                }

                Intent intentToSend;

                if (directCaptureIntents.isEmpty()) {
                    intentToSend = documentIntent;
                } else {
                    Intent chooserIntent = Intent.createChooser(documentIntent, "Choose an action");
                    chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, directCaptureIntents.toArray(new Parcelable[0]));
                    intentToSend = chooserIntent;
                }

                try {
                    MainActivity.this.startActivityForResult(intentToSend, MainActivity.REQUEST_SELECT_FILE);
                    filePathCallback.onReceiveValue( null );
                    return true;
                } catch (ActivityNotFoundException e) {
                    //MainActivity.this.cancelFileUpload();
                    Toast.makeText(MainActivity.this, /*R.string.*/"cannot_open_file_chooser", Toast.LENGTH_LONG).show();
                    return false;
                }
            }

            @Override
            public void onShowCustomView(View view, CustomViewCallback callback) {
                super.onShowCustomView( view, callback );
            }

            @Override
            public void onPermissionRequest(PermissionRequest request) {
                super.onPermissionRequest( request );
            }

            @Override
            public void onShowCustomView(View view, int requestedOrientation, CustomViewCallback callback) {
                super.onShowCustomView( view, requestedOrientation, callback );
            }
        } );

        mWebView.setOnTouchListener( new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return false;
            }
        } );
    }

    Uri directUploadImageUri;
    public void setDirectUploadImageUri(Uri directUploadImageUri) {
        this.directUploadImageUri = directUploadImageUri;
    }

    public static Intent getPictureFromCam(Activity activity){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(activity.getPackageManager()) != null){
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createExternalStoragePublicPicture();
                // Continue only if the File was successfully created
                if (photoFile != null) {
                    //Send the path of the picture via Intent
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,	Uri.fromFile(photoFile));
                    return takePictureIntent;
                }
            } catch (Exception ex) {
                Log.i("Picture from camera", "Error occurred while creating the File.");
            }
        }
        return null;
    }

    /**
     * Create a temporary image file in public Picture directory. It can be used to save an
     * image taken by camera app since camera app cannot access thus save image to app's
     * private folder
     * @return
     */
    public static File createExternalStoragePublicPicture() {
        // Create a path where we will place our picture in the user's
        // public pictures directory.  Note that you should be careful about
        // what you place here, since the user often manages these files.  For
        // pictures and other media owned by the application, consider
        // Context.getExternalMediaDir().
        File path = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File file = new File(path, "TEMP_IMG_FILE_NAME");

        // Make sure the Pictures directory exists.
        path.mkdirs();
        return file;
    }

    //Add this method to show Dialog when the required permission has been granted to the app.
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
//        switch (requestCode) {
//            case FilePickerDialog.EXTERNAL_READ_PERMISSION_GRANT: {
//                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                    if (dialog != null) {
//                        openFileSelectionDialog();
//                    }
//                } else {
//                    //Permission has not been granted. Notify the user.
//                    Toast.makeText(WebBrowserScreen.this, "Permission is Required for getting list of files", Toast.LENGTH_SHORT).show();
//                }
//            }
//        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() == KeyEvent.ACTION_DOWN) {
            switch (keyCode) {
                case KeyEvent.KEYCODE_BACK:
                    if (mWebView.canGoBack()) {
                        mWebView.goBack();
                    } else {
                        finish();
                    }
                    return true;
            }

        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_favorite) {
            startOCRScan();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * start ocr on scan id click
     */
    private void startOCRScan() {
        // Intent for MyScanActivity
        Intent intent = new Intent(this, MRPScanActivity.class);
        startActivityForResult(intent, MY_BLINK_ID_REQUEST_CODE);

    }

    /**
     * This method is called whenever control is returned from activity started with
     * startActivityForResult.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // onActivityResult is called whenever we are returned from activity started
        // with startActivityForResult. We need to check request code to determine
        // that we have really returned from BlinkID activity.
        if (requestCode == MY_BLINK_ID_REQUEST_CODE) {
            if (resultCode == MRPScanActivity.RESULT_OK && data != null) {

                // depending on settings, we may have multiple scan results.
                // we first need to obtain recognition results
                RecognitionResults results = data.getParcelableExtra(ScanActivity.EXTRAS_RECOGNITION_RESULTS);
                BaseRecognitionResult[] resArray = null;
                if (results != null) {
                    // get array of recognition results
                    resArray = results.getRecognitionResults();
                }
                if (resArray != null) {
                    Log.i(TAG, "Data count: " + resArray.length);
                    int i = 1;

                    for (BaseRecognitionResult res : resArray) {
                        Log.i(TAG, "Data #" + Integer.valueOf(i++).toString());

                        // Each element in resultArray inherits BaseRecognitionResult class and
                        // represents the scan result of one of activated recognizers that have
                        // been set up.

                        res.log();
                    }

                } else {
                    Log.e(TAG, "Unable to retrieve recognition data!");
                }

                data.setComponent(new ComponentName(this, ResultActivity.class));
                startActivity(data);
            }
        }
    }
}