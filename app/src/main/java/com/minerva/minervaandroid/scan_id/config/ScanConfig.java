package com.minerva.minervaandroid.scan_id.config;

import com.microblink.recognizers.blinkbarcode.usdl.USDLRecognizerSettings;
import com.microblink.recognizers.blinkid.mrtd.MRTDRecognizerSettings;
import com.microblink.recognizers.settings.RecognizerSettings;

/**
 * Created by Basanta on 1/28/18.
 */

public class ScanConfig {

    /** Returns recognizer settings */
    public static RecognizerSettings[] getRecognizerSettings() {
        MRTDRecognizerSettings sett = new MRTDRecognizerSettings();
        USDLRecognizerSettings usdlSett = new USDLRecognizerSettings();

        return new RecognizerSettings[] {
                sett, usdlSett
        };
    }
}
