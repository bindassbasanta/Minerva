package com.minerva.minervaandroid.scan_id.ocr;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.Toast;

import com.microblink.activity.ScanActivity;
import com.microblink.recognizers.BaseRecognitionResult;
import com.microblink.recognizers.RecognitionResults;
import com.microblink.recognizers.blinkid.mrtd.MRTDRecognitionResult;
import com.microblink.results.date.Date;
import com.minerva.minervaandroid.R;

import java.text.DateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;


/**
 * Created by igor on 12/2/14.
 */
public class ResultActivity extends FragmentActivity {

    FragmentPagerAdapter adapterViewPager;

    @SuppressLint("InlinedApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_result);
        ViewPager vpPager = (ViewPager) findViewById(R.id.vpPager);
        adapterViewPager = new ResultFragmentAdapter(getSupportFragmentManager());
        vpPager.setAdapter(adapterViewPager);
    }

    public void buttonClickHandler(View view) {
        final int id = view.getId();
        if (id == R.id.btnYesPay) {
            finish();
            Bundle bundle = getIntent().getExtras();
            RecognitionResults results = (RecognitionResults) bundle.get(ScanActivity.EXTRAS_RECOGNITION_RESULTS);

            BaseRecognitionResult[] dataArray = results.getRecognitionResults();

            for (BaseRecognitionResult baseResult : dataArray) {
                if (baseResult instanceof MRTDRecognitionResult) {
                    MRTDRecognitionResult result = (MRTDRecognitionResult) baseResult;
                    if (result.isValid() && !result.isEmpty()) {
                        if (result.isMRZParsed()) {
                            String primaryId = result.getPrimaryId();
                            String secondaryId = result.getSecondaryId();
                            String documentNumber = result.getDocumentNumber();

                            Calendar cal = GregorianCalendar.getInstance();
                            DateFormat df = DateFormat.getDateInstance();

                            Date dob = result.getDateOfBirth();
                            String dobString = "";
                            if (dob != null) {
                                cal.set(dob.getYear(), dob.getMonth() - 1, dob.getDay());
                                dobString = df.format(cal.getTime());
                            }

                            Date expiryDate = result.getDateOfExpiry();
                            String expiryString = "";
                            if (expiryDate != null) {
                                cal.set(expiryDate.getYear(), expiryDate.getMonth() - 1, expiryDate.getDay());
                                df = DateFormat.getDateInstance();
                                expiryString = df.format(cal.getTime());
                            }

                            String nationality = result.getNationality();
                            String issueNation = result.getIssuer();
                            String gender = result.getSex();
                            String code = result.getDocumentCode();
                            String idNo = result.getOpt1();

                            Toast.makeText(this, "Last Name: " + primaryId
                                            + "\nFirst Name:" + secondaryId
                                            + "\nGender:" + gender
                                            + "\nIssuer:" + issueNation
                                            + "\nNationality:" + nationality
                                            + "\nCode:" + code
                                            + "\nDoc No:" + documentNumber
                                            + "\nDOB:" + dobString
                                            + "\nExpiry Date:" + expiryString
                                            + "\nId No:" + idNo,
                                    Toast.LENGTH_LONG);
                        } else {
                            // attempt to parse OCR result by yourself
                            // or ask user to try again
                        }

                    } else {
                        // not all relevant data was scanned, ask user
                        // to try again
                    }
                }
            }
        } else if (id == R.id.btnNoBack) {
            finish();
        }
    }

    class ResultFragmentAdapter extends FragmentPagerAdapter {

        RecognitionResults mResults = getIntent().getExtras().getParcelable(ScanActivity.EXTRAS_RECOGNITION_RESULTS);

        public ResultFragmentAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return ResultFragment.newInstance(mResults.getRecognitionResults()[position]);
        }

        @Override
        public int getCount() {
            if (mResults.getRecognitionResults() == null) {
                return 0;
            } else {
                return mResults.getRecognitionResults().length;
            }
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mResults.getRecognitionResults()[position].getTitle();
        }
    }
}