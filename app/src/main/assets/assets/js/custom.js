var app = {
    init: function() {
        FastClick.attach(document.body);
        this.setHeight(); // This sets the height of page according to the viewport
        this.animateSplash(); // Animates the splash page
        this._check(); // Checks the bank/wallet checkboxes and/or radios on page load
        this._checkLoanStatus();
        this.toggleLoginStatus();
        this.toggleAccount(); // On pages where we have to select `Bank` or `wallet`. Handle the switches
        this.toggleLoanSelect();
        this.modalHelpers(); // All modals are triggerd from this functions
        this.dynamicModals(); // Show dialog box on the Profile summary page when clicked
        // this.circularAvatar();
        this.showModalHelper();
        this.bankForm();
        this.showBankStatement();
        this.toggleOtherSource();
        this._loanVisibility();
        this.toggleLoanVisibility();
    },

    _loanVisibility: function () {
        if ($('#crd-type').val() == 'emi') {
            $('.loan-durt-group').fadeIn();
        }
        else {
            $('.loan-durt-group').fadeOut()
        }
    },

    toggleLoanVisibility: function () {
        var self = this;
        $('#crd-type').on('change', function(){
            self._loanVisibility();
        });
    },

    toggleOtherSource: function () {
        $('#income-src').on('change', function(){
            var selectedOption = ($('#income-src option:selected').val());
            if (selectedOption.toLowerCase() == 'other') {
                $('.other-source').fadeIn();
            }
            else {
                 $('.other-source').fadeOut();
            }
        })
    },

    dynamicModals: function() {

        //  The user clicks the register button
        $('#button-register').on('click', function() {
            // Set showRegisterModal to 1 
            localStorage.setItem('showRegisterModal', 1);
            localStorage.setItem('hasCompletedProfile', 1);
            localStorage.showVal = 0;
            window.location.replace('my-profile.html');
            localStorage.setItem('disableCards', 0)
        });


        $('#hurray-message').on('click', function(e) {
            e.preventDefault();
            // Set showRegisterModal to 1 
            var destination = $(this).attr('data-redirect');
            console.log(destination)
            if (localStorage.hasCompletedProfile == 1) {

                localStorage.setItem('showHurrayModal', 1);
                window.location.replace(destination);


                localStorage.setItem('showVal', '1')
                localStorage.hasCompletedProfile = 0;
            } else {
                $('#success-dialog-profile').modal('show');
            }
        });

        $('#loan-message').on('click', function(e) {
            e.preventDefault();
            var destination = $(this).attr('data-redirect');

            localStorage.setItem('disableCards', 1);
            localStorage.setItem('showHurrayModal', 1);

            window.location.replace(destination);
        });

    },

    showModalHelper: function() {
        if (localStorage.showRegisterModal == 1 && window.location.pathname.split("/").pop() == 'my-profile.html') {
            //  Register Modal
            $('#register-dialog').modal('show');
            localStorage.showRegisterModal = 0;
        }

        if (localStorage.disableCards == 1 && window.location.pathname.split("/").pop() == 'dashboard.html') {
            //  Register Modal
            $('.dashcard').each(function() {
                $(this).removeClass('is-disabled');
            })
        }



        // Hurray Modal

        if (localStorage.showHurrayModal == 1 && window.location.pathname.split("/").pop() == 'dashboard.html') {
            $('#hurray-modal').modal('show');
            localStorage.showHurrayModal = 0;

        }

        if (localStorage.hasCompletedProfile == 1 && window.location.pathname.split("/").pop() == 'my-profile.html') {
            $('.completion-status').text('0% Complete');
            $('.profile-details').text('Complete your profile and increase your credit limit');
            $('.progress-bar').css('width', '0%');
            $('.profile-name').empty();
            $('.action-container:not(.has-completed').addClass('under-progress');
            $(".avatar").imgProgressTo(0);

            $('.imgProgress-img').css('background-image', 'url(http://www.fishersrotary.org/websites_core/images/no_image.jpg)');
            // localStorage.hasCompletedProfile = 0;
        }

        if (localStorage.hasCompletedProfile == 1 && window.location.pathname.split("/").pop() == 'social-media.html') {
            $('.ci-check input[type=checkbox]').removeAttr('checked')
        }
        if (+localStorage.showVal != 1) {
            $('.general-form input[type=text]').val('')
        }



    },

    setHeight: function() {
        var winheight = $(window).height();
        $('.app-bg').css({ 'height': winheight + 'px' })
    },

    animateSplash: function() {

        $('.login-choice .btn').click(function() {
            $('.logo-animateable').css({ 'top': '6%' });
            $('.login-choice').fadeOut(100);
            // $('.cinergy-form-animateable').css({ 'opacity': '1' })
            $('.cinergy-form-float').fadeIn()
            // cinergy-form-float
        })
    },
    _check: function() {
        var firstChecked = $("#radio-1").prop("checked");
        if (firstChecked == true) {
            $('#bankForm').show();
            $('#walletForm').hide();
            // $('.account-text').text('Bank Statement');
            $('#bank-wallet-label').text('Bank A/C No.');
            $('#balance-amt-label').text('Average Monthly Bank Balance:');
        } else if (firstChecked == false) {
            $('#bankForm').hide();
            $('#walletForm').show();
            // $('.account-text').text('Wallet Details');
            $('#bank-wallet-label').text('Wallet ID No.');
            $('#balance-amt-label').text('Average Monthly Wallet Balance:');
            // $('#bank-wallet-statement').attr('placeholder','Wallet ID').val("").focus().blur();

        } else {

            $('#bankForm').hide();
            $('#walletForm').hide();
        }
    },
    _checkLoanStatus: function() {
        var firstChecked = $("#has-loan").prop("checked");
        if (firstChecked == true) {
            $('.has-loan-form').show();
            // $('#walletForm').hide();
            // $('.account-text').text('Bank Statement');
        } else if (firstChecked == false) {
            $('.has-loan-form').hide();
            // $('#walletForm').show();
            // $('.account-text').text('Wallet Details');
            // $('#bank-wallet-label').text('Wallet ID');
            // $('#bank-wallet-statement').attr('placeholder','Wallet ID').val("").focus().blur();

        }
    },

    _checkLoanSelect: function() {
        var checkSelect = $('#loan-selector').find(":selected").val()
        if (checkSelect == 'bank-loan') {
            $('#bankloanForm').show();
            $('#walletloanForm').hide();
        } else if (checkSelect == 'wallet-loan') {
            $('#bankloanForm').hide();
            $('#walletloanForm').show();

        } else {
            $('#bankloanForm').hide();
            $('#walletloanForm').hide();
        }
    },

    toggleAccount: function() {
        $('input[type=radio][name=account-radio]').on('change', function(e) {
            app._check();
        });
    },


    toggleLoginStatus: function() {
        $('input[type=radio][name=radio-loan]').on('change', function(e) {
            app._checkLoanStatus();
        });
    },


    toggleLoanSelect: function() {
        $('#loan-selector').on('change', function() {
            app._checkLoanSelect()
        })
    },

    modalHelpers: function() {

        $('#use-loan, .transfer').on('click', function(e) {
            e.preventDefault();
            $('#success-dialog').modal('show')
        });

        $('.profile-success').on('click', function(e) {
            e.preventDefault();
            $('#success-dialog-profile').modal('show')
        });
        $('#profile-modal').modal('show')

        $('.reset-sent').on('click', function(e) {
            e.preventDefault();
            $('#reset-dialog').modal('show')
        });


        $('#prepay-loans').on('click', function(e) {
            e.preventDefault();
            $('#dialog').modal('show')
        });




        // go-to-tab2
        // $('.go-to-tab2').on('click', function(e){
        //     $('#tab2-click').trigger('click');
        // });

        var host = location.hostname;
        var getUrl;
        var cwd;

        if (host == 'localhost' || host == '127.0.0.1') {
            var cwd = `http://${window.location.host}/`;
        } else {
            cwd = 'http://tech101.co.uk/demo/minerva/'
        }
        

        $('.autohide').on('show.bs.modal', function() {
            var myModal = $(this);
            getUrl = $(this).attr('data-redirect');
            clearTimeout(myModal.data('hideInterval'));
            myModal.data('hideInterval', setTimeout(function() {
                myModal.modal('hide');
                if (getUrl != undefined) { window.location.replace(cwd + getUrl) };
            }, 2500));
        });


        $('#take-loan-btn').on('click', function(e) {
            e.preventDefault();
            var tbp = +$('#to-be-paid').val();
            var limit = +$(this).attr('data-limit');
            if (tbp > limit) {

                $('#take-loan-modal').modal('show');
            } else {

                $('#success-dialog').modal('show');
            }
        })





        // Handle register modal
        this.showModalHelper();
    }, //The  modalHelper function ends here


    circularAvatar: function() {
        // body...

        $avatar = $('.avatar');
        var fgcolor = $avatar.attr('data-fgcolor');
        var bgcolor = $avatar.attr('data-bgcolor');
        var avatar = $avatar.attr('data-avatar');
        var size = $avatar.attr('data-size');
        var barSize = $avatar.attr('data-barsize');
        var percentage = +$avatar.attr('data-percentage');

        $(".avatar").imgProgress({

            // path to the image
            img_url: avatar,

            // size in pixels
            size: size,

            // bar size
            barSize: barSize,

            // background color
            backgroundColor: bgcolor,

            // foreground color
            foregroundColor: fgcolor,

            // CSS background-size property
            backgroundSize: 'cover',

            // current percentage value
            percent: percentage

        });

    },



    bankForm: function() {


        var uploading = `<i class="fa fa-circle-o-notch fa-spin"></i> Uploading`;
        var upload = `<i class="fa fa-upload"></i> Upload`;

        var currentPage = location.pathname.substring(location.pathname.lastIndexOf("/") + 1);
        if (currentPage != 'income-statement.html' && currentPage != 'HSBC.html' && currentPage != 'Mandiri.html' && currentPage != 'BCA.html') {
            localStorage.removeItem('getBank')
        }

        if (localStorage.getBank != undefined) {

            $('.success-message').fadeIn();
            $('.success-accept-button').attr('href', localStorage.getBank + '.html');
            $('#bank-name').text(localStorage.getBank);

            // localStorage.setItem('getBank',localStorage.getBank)
        }

        $('.success-delete-button').on('click', function(e) {
            e.preventDefault();
            localStorage.removeItem('getBank');
            $('.success-message').fadeOut();
        })

        $('.btn-bank-reject').on('click', function() {
            localStorage.removeItem('getBank');
            location.replace('income-statement.html')
        })

        $('.btn-bank-accept').on('click', function(e) {
            e.preventDefault()
            window.history.back();
        })

        function sendPic() {
            var form = new FormData();
            form.append("file", $bankbutton[0].files[0]);
            // alert($bankbutton[0].files[0])
            canvasHelper.imageToData($bankbutton[0].files[0]);
            var settings = {
                "async": true,
                "crossDomain": true,
                "url": "https://secret-meadow-30989.herokuapp.com/",
                "method": "POST",
                "headers": {},
                "processData": false,
                "contentType": false,
                "mimeType": "multipart/form-data",
                "data": form
            }


            $.ajax(settings).done(function(response) {
                // alert(response);


                var bankResponse = JSON.parse(response)[0]
                if (bankResponse != 'Unknown') {
                    $('.success-message').fadeIn();
                    $('#bank-name').text(bankResponse);
                    $('.success-accept-button').attr('href', bankResponse + '.html');

                    localStorage.setItem('getBank', bankResponse)
                } else {
                    $('.error-message').fadeIn();
                    setTimeout(function() {
                        $('.success-message').hide();
                        $('.error-message').fadeOut();
                    }, 4000)
                }
                // $('.success-message').append(response)

                // $('.success-message').css('background-image', 'url('+localStorage.bankImage+')')
            }).fail(function() {
                $('.error-message').fadeIn();
                $('.error-message').text('An error encountered during the file read. Please retry');
                setTimeout(function() {

                    $('.success-message').hide();
                    $('.error-message').fadeOut();
                }, 4000)
            })


            .always(function() {

                $('.uploading').html(upload)
            });

        }







        var $bankbutton = $('#bnkform');
        $('#bnkform').on('change', function() {
            $('.uploading').html(uploading)
            sendPic()
        });

    },
    showBankStatement: function() {
        $('.btn-bank-accept').on('click', function() {
            localStorage.setItem('hasAcceptedStatement', 'true');
        })
    }




}



var canvasHelper = {
    imageToData: function(img) {
        var reader = new FileReader();
        reader.readAsDataURL(img);
        reader.onload = function() {
            localStorage.setItem('bankImage', reader.result);
        };
        reader.onerror = function(error) {
            // console.log('Error: ', error);
        };

    },

    dataToImage: function() {
        var data = localStorage.getItem('imgData')
        return "data:image/png;base64," + data
    }
}

$(document).ready(function() {
    app.init();
    //$('input.cb-value').prop("checked", true);
    $('.cb-value').click(function() {
        var mainParent = $(this).parent('.toggle-btn');
        if ($(mainParent).find('input.cb-value').is(':checked')) {
            $(mainParent).addClass('active');
        } else {
            $(mainParent).removeClass('active');
        }

    });

})
